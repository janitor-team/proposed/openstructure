From b2798f24c37cb2ff0f46b929a4d41b4f3ce54a68 Mon Sep 17 00:00:00 2001
From: Gabriel Studer <gabriel.studer@unibas.ch>
Date: Mon, 2 May 2022 14:48:38 +0200
Subject: [PATCH] Only add interactions with non-zero force constant in
 TopologyCreator

Avoids triggering errors with more strict parameter checks introduced
in OpenMM 7.4
---
 modules/mol/mm/src/topology_creator.cc | 72 +++++++++++++++++---------
 1 file changed, 48 insertions(+), 24 deletions(-)

diff --git a/modules/mol/mm/src/topology_creator.cc b/modules/mol/mm/src/topology_creator.cc
index c80ec9da..dede191f 100644
--- a/modules/mol/mm/src/topology_creator.cc
+++ b/modules/mol/mm/src/topology_creator.cc
@@ -560,7 +560,9 @@ TopologyPtr TopologyCreator::Create(ost::mol::EntityHandle& ent,
           bonds.erase(bond_index);
           //There are only harmonic bonds supported
           parameters = (*j)->GetParam();
-          top->AddHarmonicBond(one,two,parameters[0],parameters[1]);
+          if(parameters[1] != 0.0) {
+            top->AddHarmonicBond(one,two,parameters[0],parameters[1]);
+          }
         }
       }
     }
@@ -585,7 +587,9 @@ TopologyPtr TopologyCreator::Create(ost::mol::EntityHandle& ent,
       }
 
       parameters = bond_ptr->GetParam();
-      top->AddHarmonicBond((*i)[0],(*i)[1],parameters[0],parameters[1]);
+      if(parameters[1] != 0.0) {
+        top->AddHarmonicBond((*i)[0],(*i)[1],parameters[0],parameters[1]);
+      }
     }
   }
 
@@ -611,14 +615,18 @@ TopologyPtr TopologyCreator::Create(ost::mol::EntityHandle& ent,
           parameters = (*j)->GetParam();
           switch((*j)->GetFuncType()){
             case HarmonicAngle:{
-              top->AddHarmonicAngle(angle_index[0],angle_index[1],angle_index[2],
-                                     parameters[0], parameters[1]);
+              if(parameters[1] != 0.0) {
+                top->AddHarmonicAngle(angle_index[0],angle_index[1],angle_index[2],
+                                      parameters[0], parameters[1]);
+              }
               break;
             }
             case UreyBradleyAngle:{
-              top->AddUreyBradleyAngle(angle_index[0],angle_index[1],angle_index[2],
-                                        parameters[0], parameters[1], parameters[2],
-                                        parameters[3]);
+              if(parameters[1] != 0.0 || parameters[3] != 0.0) {
+                top->AddUreyBradleyAngle(angle_index[0],angle_index[1],angle_index[2],
+                                          parameters[0], parameters[1], parameters[2],
+                                          parameters[3]);
+              }
               break;
             }
             default:{
@@ -653,14 +661,18 @@ TopologyPtr TopologyCreator::Create(ost::mol::EntityHandle& ent,
       parameters = angle_ptr->GetParam();
       switch(angle_ptr->GetFuncType()){
         case HarmonicAngle:{
-          top->AddHarmonicAngle((*i)[0],(*i)[1],(*i)[2],
-                                 parameters[0],parameters[1]);
+          if(parameters[1] != 0.0) {
+            top->AddHarmonicAngle((*i)[0],(*i)[1],(*i)[2],
+                                   parameters[0],parameters[1]);
+          }
           break;
         }
         case UreyBradleyAngle:{
-          top->AddUreyBradleyAngle((*i)[0],(*i)[1],(*i)[2],
-                                    parameters[0],parameters[1],
-                                    parameters[2],parameters[3]);
+          if(parameters[1] != 0.0 || parameters[3] != 0.0) {
+            top->AddUreyBradleyAngle((*i)[0],(*i)[1],(*i)[2],
+                                      parameters[0],parameters[1],
+                                      parameters[2],parameters[3]);
+          }
           break;
         }
         default:{
@@ -706,8 +718,10 @@ TopologyPtr TopologyCreator::Create(ost::mol::EntityHandle& ent,
           dihedrals_to_delete.insert(dihedral_index);
           //only periodic dihedrals are supported... 
           parameters = (*j)->GetParam();
-          top->AddPeriodicDihedral(one,two,three,four,
-                                   parameters[0],parameters[1],parameters[2]);
+          if(parameters[2] != 0.0) {
+            top->AddPeriodicDihedral(one,two,three,four,
+                                     parameters[0],parameters[1],parameters[2]);
+          }
         }
       }
     }
@@ -745,8 +759,10 @@ TopologyPtr TopologyCreator::Create(ost::mol::EntityHandle& ent,
       for(std::vector<InteractionPtr>::iterator j = dihedral_ptr.begin();
           j != dihedral_ptr.end(); ++j){
         parameters = (*j)->GetParam();
-        top->AddPeriodicDihedral((*i)[0],(*i)[1],(*i)[2],(*i)[3],
-                                  parameters[0],parameters[1],parameters[2]);
+        if(parameters[2] != 0.0) {
+          top->AddPeriodicDihedral((*i)[0],(*i)[1],(*i)[2],(*i)[3],
+                                    parameters[0],parameters[1],parameters[2]);
+        }
       }
     }
   }
@@ -772,13 +788,17 @@ TopologyPtr TopologyCreator::Create(ost::mol::EntityHandle& ent,
           parameters = (*j)->GetParam();
           switch((*j)->GetFuncType()){
             case PeriodicImproper:{
-              top->AddPeriodicImproper(one,two,three,four,
-                                       parameters[0],parameters[1],parameters[2]);
+              if(parameters[2] != 0.0) {
+                top->AddPeriodicImproper(one,two,three,four,
+                                         parameters[0],parameters[1],parameters[2]);
+              }
               break;
             }
             case HarmonicImproper:{
-              top->AddHarmonicImproper(one,two,three,four,
-                                       parameters[0],parameters[1]);
+              if(parameters[1] != 0.0) {
+                top->AddHarmonicImproper(one,two,three,four,
+                                         parameters[0],parameters[1]);
+              }
               break;
             }
             default:{
@@ -825,13 +845,17 @@ TopologyPtr TopologyCreator::Create(ost::mol::EntityHandle& ent,
         parameters = (*j)->GetParam();
         switch((*j)->GetFuncType()){
           case PeriodicImproper:{
-            top->AddPeriodicImproper((*i)[0],(*i)[1],(*i)[2],(*i)[3],
-                                      parameters[0],parameters[1],parameters[2]);
+            if(parameters[2] != 0.0) {
+              top->AddPeriodicImproper((*i)[0],(*i)[1],(*i)[2],(*i)[3],
+                                        parameters[0],parameters[1],parameters[2]);
+            }
             break;
           }
           case HarmonicImproper:{
-            top->AddHarmonicImproper((*i)[0],(*i)[1],(*i)[2],(*i)[3],
-                                      parameters[0],parameters[1]);
+            if(parameters[1] != 0.0) {
+              top->AddHarmonicImproper((*i)[0],(*i)[1],(*i)[2],(*i)[3],
+                                        parameters[0],parameters[1]);
+            }
             break;
           }
           default:{
-- 
GitLab

